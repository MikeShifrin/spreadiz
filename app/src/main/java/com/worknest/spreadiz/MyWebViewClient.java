package com.worknest.spreadiz;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;


public class MyWebViewClient extends WebViewClient {

    private ProgressBar mProgressBar;

    public MyWebViewClient(ProgressBar progressBar){
        mProgressBar = progressBar;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return Uri.parse(url).getHost().equals(Constants.Links.HOME_PAGE) || super.shouldOverrideUrlLoading(view, url);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        mProgressBar.setVisibility(View.VISIBLE);
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        view.loadUrl("javascript:(function() { " +
                "document.getElementsByClassName('site-header')[0].style.display='none'; })()");
        view.loadUrl("javascript:(function() { " +
                "document.getElementsByClassName('site-footer')[0].style.display='none'; })()");
        mProgressBar.setVisibility(View.GONE);
        super.onPageFinished(view, url);
    }
}
