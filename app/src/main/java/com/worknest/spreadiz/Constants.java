package com.worknest.spreadiz;

/**
 * Created by arina on 12.12.17.
 */

public final class Constants {

    public static final class Links{
        public static final String HOME_PAGE = "https://spreadiz.com/";
        public static final String LOGIN_PAGE = "https://spreadiz.com/vendor-dashboard/?view=login";
        public static final String POPULAR_PAGE = "https://spreadiz.com/popular-items/";
        public static final String PROFILE_PAGE = "https://spreadiz.com/vendor-dashboard/";
    }
    public static final class Instagram{
        public static final String AUTH_URI = "https://api.instagram.com/oauth/authorize/";
        public static final String TOKEN_URI = "https://api.instagram.com/oauth/access_token";
        public static final String API_URI = "https://api.instagram.com/v1";
        public static final String CALLBACK_URI = "https://spreadiz1.api.oneall.com/socialize/callback.html";
        public static final String CLIENT_ID = "9165f2e46aa94eb89b63b36309796ce5";
        public static final String CLIENT_SECRET = "e2c8e6a118494eb4a16fe852da394ab3";


        public static final String AUTH_URL_REQUEST = AUTH_URI
                + "?client_id="
                + CLIENT_ID
                + "&amp;amp;redirect_uri="
                + CALLBACK_URI
                + "&amp;amp;response_type=code&amp;amp;display=touch&amp;amp;scope=likes+comments+relationships";

        public static final String TOKEN_URL_REQUEST = TOKEN_URI
                +"?client_id="
                + CLIENT_ID
                +"&amp;amp;client_secret="
                + CLIENT_SECRET
                + "&amp;amp;redirect_uri="
                + CALLBACK_URI+"&amp;amp;grant_type=authorization_code";

        public static final String GET_ACCES_TOKEN = "access_token";
        public static final String GET_USER_ID = "id";
        public static final String GET_USER_NAME = "username";
        public static final String GET_FULL_NAME = "full_name";
        public static final String GET_AVATAR = "profile_picture";


    }
    public static final class Search{
        public static final String START_SEARCH_URL = "https://spreadiz.com/?s=";
        public static final String END_SEARCH_URL = "&post_type=download";
    }
}