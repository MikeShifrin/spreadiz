package com.worknest.spreadiz;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class LoginActivity extends AppCompatActivity {


    private Button mLoginButton;
    private String mToken;
    private String mAccessToken;
    private String mId, mUsername, mFullName, mAvatar;
    private InstagramConnect mInstagramConnect;
    private SharedPreferences mSharedPreferences;
    private WebView mLoginWebView;
    private AlertDialog.Builder mAlert;
    private AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mSharedPreferences = getSharedPreferences(
                getString(R.string.shared_preferences),
                Context.MODE_PRIVATE
        );

        if(isLoggedIn()){
            startMainActivity();
        }

        mAlert = new AlertDialog.Builder(LoginActivity.this);
        mAlert.setTitle("Login");

        mLoginWebView = new LoginWebView(LoginActivity.this);
        mLoginWebView.setVerticalScrollBarEnabled(false);
        mLoginWebView.setHorizontalScrollBarEnabled(false);
        mLoginWebView.getSettings().setJavaScriptEnabled(true);

        mAlert.setView(mLoginWebView);
        mDialog = mAlert.create();

        mLoginWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(url.startsWith(Constants.Instagram.CALLBACK_URI)) {
                    String parts[] = url.split("=");
                    mToken = parts[1];
                    System.out.println(mToken);
                    mInstagramConnect.execute();

                    return true;
                }
                return false;
            }
        });

        mInstagramConnect = new InstagramConnect();
        mLoginButton = findViewById(R.id.button_login);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mLoginWebView.loadUrl(Constants.Instagram.AUTH_URL_REQUEST);

                mDialog.show();

            }
        });
    }

    private void startMainActivity(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private boolean isLoggedIn(){
        boolean loggedIn = false;
        String id = mSharedPreferences.getString(getString(R.string.pref_id), null);
        String username = mSharedPreferences.getString(getString(R.string.pref_username), null);
        String access_token = mSharedPreferences.getString(getString(R.string.pref_access_token), null);
        if(id != null || username != null || access_token != null){
            loggedIn = true;
        }
        return loggedIn;
    }



    //Getting the Access Token and userdata
    @SuppressLint("StaticFieldLeak")
    public class InstagramConnect extends AsyncTask<Void, Void, Void> {


        private String getStringFromInputStream(InputStream stream) throws IOException {
            int n;
            char[] buffer = new char[1024 * 4];
            InputStreamReader reader = new InputStreamReader(stream, "UTF8");
            StringWriter writer = new StringWriter();

            while (-1 != (n = reader.read(buffer))) {
                writer.write(buffer, 0, n);
            }
            return writer.toString();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try
            {
                URL url=new URL(Constants.Instagram.TOKEN_URL_REQUEST);
                HttpsURLConnection httpsURLConnection=(HttpsURLConnection) url.openConnection();
                httpsURLConnection.setRequestMethod("POST");
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpsURLConnection.getOutputStream());
                outputStreamWriter.write("client_id=" + Constants.Instagram.CLIENT_ID +
                        "&amp;amp;client_secret=" + Constants.Instagram.CLIENT_SECRET +
                        "&amp;amp;grant_type=authorization_code" +
                        "&amp;amp;redirect_uri=" + Constants.Instagram.CALLBACK_URI +
                        "&amp;amp;code=" + mToken);
                outputStreamWriter.flush();

                String response = getStringFromInputStream(httpsURLConnection.getInputStream());
                JSONObject jsonObject =(JSONObject)new JSONTokener(response).nextValue();

                Log.e("Json", jsonObject.toString());
                SharedPreferences.Editor editor = mSharedPreferences.edit();

                mAccessToken = jsonObject.getString(Constants.Instagram.GET_ACCES_TOKEN);
                editor.putString(getString(R.string.pref_access_token), mAccessToken);
                Log.e("Json", mAccessToken);

                mId = jsonObject.getJSONObject("user").getString(Constants.Instagram.GET_USER_ID);
                editor.putString(getString(R.string.pref_id), mId);
                Log.e("Json", mId);

                mUsername = jsonObject.getJSONObject("user").getString(Constants.Instagram.GET_USER_NAME);
                editor.putString(getString(R.string.pref_username), mUsername);
                Log.e("Json", mUsername);

                mAvatar = jsonObject.getJSONObject("user").getString(Constants.Instagram.GET_AVATAR);
                editor.putString(getString(R.string.pref_profile_picture), mAvatar);
                Log.e("Json", mAvatar);

                mFullName = jsonObject.getJSONObject("user").getString(Constants.Instagram.GET_FULL_NAME);
                editor.putString(getString(R.string.pref_full_name), mFullName);
                Log.e("Json", mFullName);


                editor.commit();

            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            mDialog.dismiss();
            if(isLoggedIn()) {
                startMainActivity();
            }else Log.e("ERRROR", "SOMESING WRONG");
            return null;
        }
    }
}
