package com.worknest.spreadiz;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by arina on 14.12.17.
 */


public class LoginWebView extends WebView {
        public LoginWebView(Context context) {
            super(context);
        }

        public LoginWebView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public LoginWebView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        public LoginWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
        }

        public LoginWebView(Context context, AttributeSet attrs, int defStyleAttr, boolean privateBrowsing) {
            super(context, attrs, defStyleAttr, privateBrowsing);
        }

        @Override
        public boolean onCheckIsTextEditor() {
            return true;
        }
    }

